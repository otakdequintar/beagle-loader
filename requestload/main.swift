//
//  main.swift
//  requestload
//
//  Created by Michael Lyons on 7/4/18.
//  Copyright © 2018 NullCoreSystems. All rights reserved.
//

import Foundation

let notificationName = Notification.Name.init("Beagle Loader request to send")

if CommandLine.arguments.count > 1 {
    if let fileURL = URL(string: CommandLine.arguments[1]) {
//        Swift.print("\(fileURL.absoluteString)")
        NotificationCenter.default.post(name: notificationName, object: fileURL)
    } else {
        Swift.print("Unable to parse arguments")
    }
} else {
    Swift.print("No file argument")
}

