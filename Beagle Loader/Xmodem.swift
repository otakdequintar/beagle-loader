//
//  Xmodem.swift
//  Beagle Loader
//
//  Created by Michael Lyons on 7/1/18.
//  Copyright © 2018 NullCoreSystems. All rights reserved.
//

import Foundation

class Xmodem: NSObject {
    
    // MARK: State machine definition
    private enum TransferState {
        case idle
        case waitStart
        case waitResponse
        case packetSend
        case controlSend
    }
    
    // MARK: Public variables
    var delegate: CommunicationInterface?
    
    // MARK: Private variables
    private var currentState: TransferState = .idle
    private var sendData: Data!
    private var dataOffset: Int = 0
    private var packet = Data(count: 1029)
    private let sendQueue = DispatchQueue(label: "Send Queue")
    private var completion: ((Bool) -> ())? = nil
    
    // MARK: Interface (public) functions
    init(delegate: CommunicationInterface?) {
        self.delegate = delegate
    }
    
    /// Begin a new send (upload) transfer
    ///
    /// - Parameter data: data to send
    /// - Returns: True on success, False if unable to start transfer
    func send(_ data: Data, with handler: ((Bool) -> ())?) -> Bool {
        guard currentState == .idle else {
            Swift.print("Transfer already in progress")
            return false
        }
        guard data.count > 0 else {
            Swift.print("Transfer of zero bytes is pointless")
            return false
        }
        sendData = data
        dataOffset = 0
        packet[0] = 0x02    // STX
        packet[1] = 0
        currentState = .waitStart
        completion = handler
        return true
    }
    
    /// Cancel an in-progress transfer
    func cancel() {
        guard currentState != .idle else {
            // Nothing to cancel
            return
        }
        currentState = .idle
        sendQueue.async {
            // Send cancel <CAN> to receiver
            self.delegate?.send(Data(bytes: [0x18]))
        }
        completion?(false)
    }
    
    /// Handler for receiver responses
    ///
    /// - Parameter response: value(s) returned from receiver
    func handleResponse(response: Data) {
        // Response is single char, take last value if multiple
        let responseValue: UInt8 = response.last!
        
        switch currentState {
        case .waitStart:
            if responseValue == 0x43 {
                currentState = .packetSend
                sendQueue.async {
                    self.createPacket()
                    self.delegate?.send(self.packet)
                    self.currentState = .waitResponse
                }
            }
        case .waitResponse:
            if responseValue == 0x06 {
                // ACK - send next packet
                dataOffset += 1024
                if dataOffset < sendData.count {
                    // Send next packet
                    currentState = .packetSend
                    sendQueue.async {
                        self.createPacket()
                        self.delegate?.send(self.packet)
                        self.currentState = .waitResponse
                    }
                } else {
                    currentState = .controlSend
                    sendQueue.async {
                        // Send EOT to complete transfer
                        self.delegate?.send(Data(bytes: [4]))
                        self.currentState = .idle
                        self.completion?(true)
                    }
                }
            } else if responseValue == 0x15 {
                // NAK - resend last packet
                currentState = .packetSend
                sendQueue.async {
                    self.delegate?.send(self.packet)
                    self.currentState = .waitResponse
                }
            } else {
                // Invalid response
                Swift.print("Invalid response: \(responseValue)")
                currentState = .idle
                self.completion?(false)
            }
        case .idle:
            // Ignore input while idle
            return
        default:
            Swift.print("Unexpected value received")
            currentState = .idle
            self.completion?(false)
        }
    }
    
    // MARK: Internal functions
    private func createPacket() {
        // Header
        packet[1] += 1
        packet[2] = 255 - packet[1]
        // Payload
        let remainingBytes = sendData.count - dataOffset
        if remainingBytes >= 1024 {
            packet[3..<1027] = sendData[dataOffset..<dataOffset+1024]
        } else {
            packet[3..<1027-remainingBytes] = Data(repeating: 0x1A, count: 1024-remainingBytes)
            packet[3..<(3+remainingBytes)] = sendData[dataOffset..<sendData.count]
        }
        // CRC
        let crc = CRC.crc(data: packet[3..<1027])
        packet[1027] = UInt8(crc >> 8)
        packet[1028] = UInt8(crc & 0x00FF)
    }
    
}


