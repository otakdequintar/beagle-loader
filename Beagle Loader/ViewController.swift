//
//  ViewController.swift
//  Beagle Loader
//
//  Created by Michael Lyons on 6/17/18.
//  Copyright © 2018 NullCoreSystems. All rights reserved.
//

import Cocoa
import CoreBluetooth

class ViewController: NSViewController, CBCentralManagerDelegate, CommunicationInterfaceDelegate {
    
    // FIXME: Target parameters should be configured through UI
    let deviceName = "BeagleBone"
    
    // MARK: UI elements
    @IBOutlet weak var scanButton: NSButton!
    @IBOutlet weak var selectFileButton: NSButton!
    @IBOutlet weak var sendButton: NSButton!
    @IBOutlet weak var resetButton: NSButton!
    @IBOutlet weak var filenameText: NSTextField!
    @IBOutlet weak var peripheralText: NSTextField!
    @IBOutlet weak var busyIndicator: NSProgressIndicator!
    @IBOutlet weak var consoleText: NSTextField!
    @IBOutlet weak var clearButton: NSButton!
    
    var xmodem: Xmodem?
    var interface: DebugLoader?
    var file: URL? {
        didSet {
            if file != nil {
                filenameText.stringValue = file!.lastPathComponent
                if interface != nil {
                    DispatchQueue.main.async {
                        self.sendButton.isEnabled = true
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.sendButton.isEnabled = false
                }
            }
        }
    }
    var isConnected = false
    var isSending = false
    var centralManager: CBCentralManager!
    var connectedDevice: CBPeripheral?
//    var serialCharacteristic: CBCharacteristic? {
//        didSet {
//            if serialCharacteristic != nil {
//
//                DispatchQueue.main.async {
//                    self.peripheralText.stringValue = self.connectedDevice?.name ?? "No Name"
//                    self.resetButton.isEnabled = true
//                }
//                if file != nil {
//                    DispatchQueue.main.async {
//                        self.sendButton.isEnabled = true
//                    }
//                }
//            } else {
//                DispatchQueue.main.async {
//                    self.sendButton.isEnabled = false
//                    self.resetButton.isEnabled = false
//                    self.peripheralText.stringValue = ""
//                }
//            }
//        }
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set intial UI state
        scanButton.isEnabled = false
        sendButton.isEnabled = false
        resetButton.isEnabled = false
        busyIndicator.isDisplayedWhenStopped = false
        consoleText.stringValue = ""
        
        // Start bluetooth
        let queue = DispatchQueue(label: "Central Manager Queue")
        centralManager = CBCentralManager(delegate: self, queue: queue)
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    @IBAction func scan(_ sender: NSButton) {
        if isConnected {
            disconnect()
        } else {
            if centralManager.isScanning {
                stopScan()
            } else {
                startScan()
            }
        }
    }
    
    func startScan() {
        DispatchQueue.main.async {
            self.scanButton.title = "Stop"
            self.busyIndicator.startAnimation(nil)
        }
        centralManager.scanForPeripherals(withServices: [DebugLoader.serviceUUID], options: nil)
    }
    
    func stopScan() {
        DispatchQueue.main.async {
            self.scanButton.title = "Scan"
            self.busyIndicator.stopAnimation(nil)
        }
        centralManager.stopScan()
    }
    
    func disconnect() {
        if let peripheral = connectedDevice {
            DispatchQueue.main.async {
                self.scanButton.isEnabled = false
                // Include this in the block to avoid a race condition
                self.centralManager.cancelPeripheralConnection(peripheral)
            }
        }
    }
    
    @IBAction func selectFile(_ sender: NSButton) {
        let openPanel = NSOpenPanel()
        openPanel.allowsMultipleSelection = false
        openPanel.canChooseDirectories = false
        openPanel.canChooseFiles = true
        openPanel.resolvesAliases = true
        openPanel.message = "Select file to send"
        // FIXME: including text files to aid debug, not normally a valid file type
        openPanel.allowedFileTypes = ["bin","txt"]
        if openPanel.runModal() == NSApplication.ModalResponse.OK {
            file = openPanel.urls.first
        }
    }
    
    @IBAction func toggleReset(_ sender: NSButton) {
        if let interface = interface {
            interface.setReset(asserted: true)
            interface.setReset(asserted: false)
        }
    }
    
    @IBAction func sendFile(_ sender: NSButton) {
        selectFileButton.isEnabled = false
        sendButton.isEnabled = false
        if xmodem == nil {
            xmodem = Xmodem(delegate: interface)
        }
        // Perform send
        if let file = file {
            let fileData: Data
            do {
                fileData = try Data(contentsOf: file)
            } catch {
                Swift.print("Unable to open file: \(error.localizedDescription)")
                return
            }
            // Assert reset before transfer begins to halt any tx from target
            interface?.setReset(asserted: true)
            let sendOK = xmodem?.send(fileData, with: { (success: Bool) in
                DispatchQueue.main.async {
                    self.selectFileButton.isEnabled = true
                    self.sendButton.isEnabled = true
                    self.consoleText.stringValue = ""
                }
                self.isSending = false
            }) ?? false
            if sendOK {
                isSending = true
            }else {
                Swift.print("Failed to send")
                selectFileButton.isEnabled = true
                sendButton.isEnabled = true
            }
            // Release reset to allow bootloader to run, bootloader will signal ready to start transfer
            interface?.setReset(asserted: false)
            // FIXME: This belongs in a completion handler
//            selectFileButton.isEnabled = true
//            sendButton.isEnabled = true
        }
    }
    
    @IBAction func clearConsole(_ sender: NSButton) {
        consoleText.stringValue = ""
    }
    
    // MARK: CommunicationInterfaceDelegate
    func interface(_ interface: CommunicationInterface, didReceive data: Data) {
        if isSending {
            xmodem?.handleResponse(response: data)
        } else {
            DispatchQueue.main.async {
                if let text = String(data: data, encoding: .ascii) {
                    self.consoleText.stringValue.append(text)
                }
            }
        }
    }

    // MARK: Central Manager Delegate
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            DispatchQueue.main.async {
                self.scanButton.isEnabled = true
            }
        default:
//            xmodem?.cancel()
            DispatchQueue.main.async {
                // Reset UI State
                self.scanButton.isEnabled = false
                self.scanButton.title = "Scan"
                self.sendButton.isEnabled = false
                self.resetButton.isEnabled = false
                self.busyIndicator.stopAnimation(nil)
            }
        }
    }
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        guard let isConnectable = advertisementData[CBAdvertisementDataIsConnectable] as? Bool else { return }
        guard isConnectable else { return }
        guard nil != peripheral.name else { return  }
//        if name == deviceName {
            stopScan()
            // Begin connection
            connectedDevice = peripheral
            central.connect(peripheral, options: nil)
//        }
    }
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        DispatchQueue.main.async {
            self.scanButton.title = "Disconnect"
        }
        isConnected = true
        interface = DebugLoader(peripheral, delegate: self)
    }
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        isConnected = false
        connectedDevice = nil
        interface = nil
        isSending = false
    }
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        isConnected = false
        connectedDevice = nil
        interface = nil
        isSending = false
        DispatchQueue.main.async {
            self.scanButton.title = "Scan"
            self.scanButton.isEnabled = true
        }
    }
    
}

