//
//  DebugLoader.swift
//  Beagle Loader
//
//  Created by Michael Lyons on 7/4/18.
//  Copyright © 2018 NullCoreSystems. All rights reserved.
//

import Cocoa
import CoreBluetooth

class DebugLoader: NSObject, CBPeripheralDelegate, CommunicationInterface {
    
    // MARK: UUIDs for BLE debugger
    class var serviceUUID: CBUUID {
        get { return CBUUID(string: "0xFFE0") }
    }
    class var serialUUID: CBUUID {
        get { return CBUUID(string: "0xFFE1") }
    }
    class var resetUUID: CBUUID {
        get { return CBUUID(string: "0xFFE2") }
    }
    class var packetUUID: CBUUID {
        get { return CBUUID(string: "0xFFE3") }
    }
    
    // MARK: Instance variables
    private var peripheral: CBPeripheral?
    private var isSending: Bool = false
    weak var serialCharacteristic: CBCharacteristic?
    weak var packetCharacteristic: CBCharacteristic?
    weak var resetCharacteristic: CBCharacteristic?
    var delegate: CommunicationInterfaceDelegate?
    
    
    init(_ peripheral: CBPeripheral, delegate: CommunicationInterfaceDelegate) {
        self.peripheral = peripheral
        self.delegate = delegate
        super.init()
        peripheral.delegate = self
        peripheral.discoverServices([DebugLoader.serviceUUID])
    }
    
    func send(_ data: Data) {
        let writeSize: Int = peripheral?.maximumWriteValueLength(for: .withoutResponse) ?? 256
        var characteristic: CBCharacteristic?
        if data.count == 1029 {
            // Request to send an entire packet
            characteristic = packetCharacteristic
        } else {
            // Request to send a control char (not a packet)
            characteristic = serialCharacteristic
        }
        guard characteristic != nil else {
            return
        }
        var remainingBytes = data.count
        var index = 0
        while remainingBytes > writeSize {
            peripheral?.writeValue(data[index..<(index+writeSize)], for: characteristic!, type: .withoutResponse)
            index += writeSize
            remainingBytes -= writeSize
        }
        peripheral?.writeValue(data[index..<data.count], for: characteristic!, type: .withoutResponse)

    }
    
    func setReset(asserted: Bool) {
        if let characteristic = resetCharacteristic {
            if asserted {
                peripheral?.writeValue(Data(bytes: [1]), for: characteristic, type: .withoutResponse)
            } else {
                peripheral?.writeValue(Data(bytes: [0]), for: characteristic, type: .withoutResponse)
            }
        }
    }
    
    // MARK: Peripheral Delegate
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard error == nil else {
            Swift.print("\(error!.localizedDescription)")
            return
        }
        guard let service = peripheral.services?.first else { return }
        let characteristics = [DebugLoader.serialUUID, DebugLoader.resetUUID, DebugLoader.packetUUID]
        peripheral.discoverCharacteristics(characteristics, for: service)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        guard error == nil else {
            Swift.print("\(error!.localizedDescription)")
            return
        }
        guard let characteristics = service.characteristics else { return  }
        // FIXME: This is probably wrong
        if characteristics.count >= 1 {
            serialCharacteristic = characteristics[0]
            self.peripheral?.setNotifyValue(true, for: serialCharacteristic!)
            //            peripheral.discoverDescriptors(for: characteristics[0])
        }
        if characteristics.count >= 2 {
            resetCharacteristic = characteristics[1]
        }
        if characteristics.count >= 3 {
            packetCharacteristic = characteristics[2]
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?) {
        // Figure out how the serial characteristic works (debug only?)
        if let descriptors = characteristic.descriptors {
            for descriptor in descriptors {
                Swift.print("\(descriptor.description)")
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        guard error == nil else {
            Swift.print("\(error!.localizedDescription)")
            return
        }
        guard let value = characteristic.value else { return }
        delegate?.interface(self, didReceive: value)
    }
    
}

// TODO: Move protocols to their own implementation file
protocol CommunicationInterface {
    func send(_ data: Data)
}
protocol CommunicationInterfaceDelegate {
    func interface(_ interface:CommunicationInterface, didReceive data: Data)
}
