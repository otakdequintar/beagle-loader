//
//  CRC.swift
//  crc-swift
//
//  Created by Michael Lyons on 11/19/17.
//  Copyright © 2017 NullCoreSystems. All rights reserved.
//

import Foundation

class CRC: NSObject {
    
    // Table will be created at first use, then re-used
    static let crcTable: [UInt16] = createTable()
    
    class func crc(data: Data) -> UInt16 {
        var crc: UInt16 = 0
        var index: Int
        for byte in data {
            index = Int(((crc >> 8) ^ UInt16(byte)) & 0x00FF)
            crc = crcTable[index] ^ (crc << 8)
        }
        return crc
    }
    
    private class func createTable() -> [UInt16] {
        var table = [UInt16]()
        var crc: UInt16
        
        for index: UInt16 in 0...255 {
            crc = (index << 8)
            for _ in 0...7 {
                if (crc & 0x8000) != 0 {
                    crc = (crc << 1) ^ 0x1021
                } else {
                    crc = crc << 1
                }
            }
            table.append(crc)
        }
        return table
    }

}
